%dw 2.0
output application/java
---
payload map ( payload01 , indexOfPayload01 ) -> {
Account__c : payload01.Account__c default 0.1, 
Contract_Number__c: payload01.Contract_Number__c as String default ' ',
Description__c: payload01.Description__c,
Effective_Date__c: payload01.Effective_Date__c as String default ' ',
Expiration_Date__c: payload01.Expiration_Date__c as String default ' ',
Product_Code__c: payload01.Product_Code__c as String default ' ',
Item_Price_Group__c: payload01.Item_Price_Group__c as String default ' ',
Colour_Family_Code__c: payload01.Colour_Family_Code__c as String default ' ',
List_Price__c: payload01.List_Price__c,
Active__c: payload01.Active__c as String default ' ',
ITM: payload01.ITM,
DRAW: payload01.DRAW as String default ' ',
UOM: payload01.UOM,
ALPH: payload01.ALPH as String default ' ',
Tier: payload01.Tier as String default ' ',
AITM: payload01.AITM as String default ' ',
CITM: payload01.CITM as String default ' ',
DSC2: payload01.DSC2 as String default ' ',
DSC1: payload01.DSC1 as String default ' ',
SPR8: payload01.SPR8 as String default ' ',
SOM: payload01.SOM default 0,
SQ: payload01.SQ default 0,
ADCRCD: payload01.ADCRCD,
SRP5: payload01.SRP5,
UPMJ: payload01.UPMJ
}