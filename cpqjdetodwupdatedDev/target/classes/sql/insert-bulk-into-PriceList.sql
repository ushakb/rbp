INSERT INTO PriceList (Account__c,Contract_Number__c,Description__c,Effective_Date__c,Expiration_Date__c,Product_Code__c,
Item_Price_Group__c,Colour_Family_Code__c,List_Price__c,Active__c,ITM,DRAW,UOM,ALPH,Tier,AITM,CITM,
DSC2,DSC1,SPR8,SOM,SQ,ADCRCD,SRP5,UPMJ
) VALUES (:Account__c,:Contract_Number__c,:Description__c,:Effective_Date__c,:Expiration_Date__c,:Product_Code__c,
:Item_Price_Group__c,:Colour_Family_Code__c,:List_Price__c,:Active__c,:ITM,:DRAW,:UOM,:ALPH,:Tier,:AITM,:CITM,
:DSC2,:DSC1,:SPR8,:SOM,:SQ,:ADCRCD,:SRP5,:UPMJ);
